package pe.appdelasolidaridad.appdelasolidaridad;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import pe.appdelasolidaridad.appdelasolidaridad.fragments.LoginFragment;
import pe.appdelasolidaridad.appdelasolidaridad.services.SolidaridadAppService;

/**
 * Created by mconcepcion on 01/12/2016.
 */

public class SolidaridadAppApp extends Application {
    static SolidaridadAppApp instance;
    SolidaridadAppService service = new SolidaridadAppService();

    public SolidaridadAppApp() {
        super();
        instance = this;
    }

    public static SolidaridadAppApp getInstance() {
        return instance;
    }
    public SolidaridadAppService getService() {
        return service;
    }

}
