package pe.appdelasolidaridad.appdelasolidaridad.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;

import pe.appdelasolidaridad.appdelasolidaridad.R;
import pe.appdelasolidaridad.appdelasolidaridad.SolidaridadAppApp;
import pe.appdelasolidaridad.appdelasolidaridad.fragments.AboutFragment;
import pe.appdelasolidaridad.appdelasolidaridad.fragments.ChooseCategoryFragment;
import pe.appdelasolidaridad.appdelasolidaridad.fragments.FavoritiesFragment;
import pe.appdelasolidaridad.appdelasolidaridad.fragments.HomeFragment;
import pe.appdelasolidaridad.appdelasolidaridad.fragments.LoginFragment;
import pe.appdelasolidaridad.appdelasolidaridad.fragments.MyAccountFragment;
import pe.appdelasolidaridad.appdelasolidaridad.fragments.ReportFragment;
import pe.appdelasolidaridad.appdelasolidaridad.models.Session;

import static pe.appdelasolidaridad.appdelasolidaridad.R.id.nameSessionTextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView = null;
    Toolbar toolbar = null;

    TextView nameSessionTextView;
    TextView emailSessionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View hView = navigationView.getHeaderView(0);
        nameSessionTextView = (TextView) hView.findViewById(R.id.nameSessionTextView);
        emailSessionTextView = (TextView) hView.findViewById(R.id.emailSessionTextView);

        checkSession();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displaySelectedScreen(item.getItemId());
        return true;
    }

    private void displaySelectedScreen(int itemId){
        Fragment fragment = null;
        switch (itemId) {
            case R.id.homeItemNav:
                checkSession();
                break;
            case R.id.myaccountItemNav:
                fragment = new MyAccountFragment();
                break;
            case R.id.favoritesItemNav:
                fragment = new FavoritiesFragment();
                break;
            case R.id.logoutItemNav:

                logout();


                break;
            case R.id.aboutItemNav:
                fragment = new AboutFragment();
                break;
        }
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_main, fragment)
                    .addToBackStack(null)
                    .commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void logout(){


        Session session = new Session(false,null);
        SharedPreferences sharedpreferences = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        Gson gson = new Gson();
        editor.putString("Session", gson.toJson(session));
        editor.commit();

        SolidaridadAppApp.getInstance().getService().setCurrentSession(session);
        nameSessionTextView.setText("");
        emailSessionTextView.setText("");
        navigationView.getMenu().getItem(1).setVisible(false);
        navigationView.getMenu().getItem(2).setVisible(false);
        navigationView.getMenu().getItem(3).setVisible(false);

        Fragment fragment = new LoginFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void logged(Session session) {
        SharedPreferences sharedpreferences = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        Gson gson = new Gson();
        editor.putString("Session", gson.toJson(session));
        editor.commit();

        SolidaridadAppApp.getInstance().getService().setCurrentSession(session);
        nameSessionTextView.setText(session.getUser().getName() + " " + session.getUser().getLastName());
        emailSessionTextView.setText(session.getUser().getEmail());
        navigationView.getMenu().getItem(1).setVisible(true);
        navigationView.getMenu().getItem(2).setVisible(true);
        navigationView.getMenu().getItem(3).setVisible(true);

        Fragment fragment = new HomeFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main, fragment)
                .commit();
    }

    public void checkSession() {
        SharedPreferences sharedpreferences = getPreferences(Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedpreferences.getString("Session", "");
        Session session = gson.fromJson(json, Session.class);
        SolidaridadAppApp.getInstance().getService().setCurrentSession(session);
        if (null == session) {
            logout();
        } else {
            if (session.isLogged()) {
                logged(session);
            } else {
                logout();
            }
        }


    }

}
