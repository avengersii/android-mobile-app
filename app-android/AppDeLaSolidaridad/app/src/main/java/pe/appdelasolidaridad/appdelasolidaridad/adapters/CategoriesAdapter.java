package pe.appdelasolidaridad.appdelasolidaridad.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pe.appdelasolidaridad.appdelasolidaridad.R;
import pe.appdelasolidaridad.appdelasolidaridad.SolidaridadAppApp;
import pe.appdelasolidaridad.appdelasolidaridad.fragments.MapFragment;
import pe.appdelasolidaridad.appdelasolidaridad.models.Category;

/**
 * Created by mconcepcion on 01/12/2016.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {
    List<Category> categories;
    private FragmentActivity activity;

    public void setCategories(List<Category> categories) { this.categories = categories; }
    public void setActivity(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.card_category, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CategoriesAdapter.ViewHolder holder, final int position) {
        holder.descriptionTextView.setText(categories.get(position).getDescription());
        holder.categoryCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SolidaridadAppApp.getInstance().getService().setCurrentCategory(categories.get(position));
                Fragment fragment = new MapFragment();
                activity
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, fragment)
                        .commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView categoryCardView;
        TextView descriptionTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            categoryCardView = (CardView) itemView.findViewById(R.id.categoryCardView);
            descriptionTextView = (TextView) itemView.findViewById(R.id.descriptionTextView);
        }
    }
}
