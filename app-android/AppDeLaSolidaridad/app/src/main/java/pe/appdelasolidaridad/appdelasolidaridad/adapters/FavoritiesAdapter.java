package pe.appdelasolidaridad.appdelasolidaridad.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidnetworking.widget.ANImageView;

import java.util.List;

import pe.appdelasolidaridad.appdelasolidaridad.R;
import pe.appdelasolidaridad.appdelasolidaridad.SolidaridadAppApp;
import pe.appdelasolidaridad.appdelasolidaridad.fragments.ReportFragment;
import pe.appdelasolidaridad.appdelasolidaridad.models.Report;

/**
 * Created by Martel on 05/12/2016.
 */

public class FavoritiesAdapter extends RecyclerView.Adapter<FavoritiesAdapter.ViewHolder> {
    List<Report> favorities;
    private FragmentActivity activity;

    public void setFavorities(List<Report> favorities) { this.favorities = favorities; }
    public void setActivity(FragmentActivity activity) { this.activity = activity; }

    @Override
    public FavoritiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.card_report, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FavoritiesAdapter.ViewHolder holder, final int position) {
        holder.pictureReportANImageView.setDefaultImageResId(R.mipmap.ic_launcher);
        holder.pictureReportANImageView.setErrorImageResId(R.mipmap.ic_launcher);
        holder.pictureReportANImageView.setImageUrl(favorities.get(position).getImage());
        holder.descriptionReportTextView.setText(favorities.get(position).getDescription());
        holder.datetimeReportTextView.setText(favorities.get(position).getDateTime());
        holder.reportCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SolidaridadAppApp.getInstance().getService().setCurrentReport(favorities.get(position));
                Fragment fragment = new ReportFragment();

                activity.getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.content_main, fragment)
                        .commit();

            }
        });
    }

    @Override
    public int getItemCount() {
        return favorities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView reportCardView;
        ANImageView pictureReportANImageView;
        TextView descriptionReportTextView;
        TextView datetimeReportTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            reportCardView = (CardView) itemView.findViewById(R.id.reportCardView);
            pictureReportANImageView = (ANImageView) itemView.findViewById(R.id.pictureReportANImageView);
            descriptionReportTextView = (TextView) itemView.findViewById(R.id.descriptionReportTextView);
            datetimeReportTextView = (TextView) itemView.findViewById(R.id.datetimeReportTextView);
        }
    }
}
