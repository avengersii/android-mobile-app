package pe.appdelasolidaridad.appdelasolidaridad.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import pe.appdelasolidaridad.appdelasolidaridad.R;
import pe.appdelasolidaridad.appdelasolidaridad.services.SolidaridadAppService;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateUserFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    EditText emailEditText;
    EditText passwordEditText;
    EditText nameEditText;
    EditText lastNameEditText;
    EditText cellphoneEditText;
    Button saveButton;
    String TAG = "Create user";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public CreateUserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateUserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateUserFragment newInstance(String param1, String param2) {
        CreateUserFragment fragment = new CreateUserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_user, container, false);

        emailEditText = (EditText) view.findViewById(R.id.emailEditText);
        passwordEditText = (EditText) view.findViewById(R.id.passwordEditText);
        nameEditText = (EditText) view.findViewById(R.id.nameEditText);
        lastNameEditText = (EditText) view.findViewById(R.id.lastNameEditText);
        cellphoneEditText = (EditText) view.findViewById(R.id.cellphoneEditText);
        saveButton = (Button) view.findViewById(R.id.saveButton);



        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG,"Holaaaaaaaaaaaaa");
                JSONObject jsonObjectRequest = new JSONObject();
                try {
                    jsonObjectRequest.put("email", emailEditText.getText());
                    jsonObjectRequest.put("password", passwordEditText.getText());
                    jsonObjectRequest.put("name", nameEditText.getText());
                    jsonObjectRequest.put("lastname", lastNameEditText.getText());
                    jsonObjectRequest.put("cellphone", cellphoneEditText.getText());
                    jsonObjectRequest.put("tokeApi", "e74821510arcwe411a");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                AndroidNetworking.post(SolidaridadAppService.USERS_URL)
                        .setTag(TAG)
                        .addJSONObjectBody(jsonObjectRequest)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d(TAG,"Chaooooo");
                                Fragment fragment = new LoginFragment();
                                getActivity()
                                        .getSupportFragmentManager()
                                        .beginTransaction()
                                        .addToBackStack(null)
                                        .replace(R.id.content_main, fragment)
                                        .commit();
                            }

                            @Override
                            public void onError(ANError anError) {
                                Log.d(TAG, "Error: " + anError.getErrorBody());
                            }
                        });


            }
        });

        return view;
    }

}
