package pe.appdelasolidaridad.appdelasolidaridad.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import pe.appdelasolidaridad.appdelasolidaridad.R;
import pe.appdelasolidaridad.appdelasolidaridad.adapters.FavoritiesAdapter;
import pe.appdelasolidaridad.appdelasolidaridad.models.Report;
import pe.appdelasolidaridad.appdelasolidaridad.services.SolidaridadAppService;
import pe.appdelasolidaridad.appdelasolidaridad.sqlite.FavoritesSQLite;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavoritiesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavoritiesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoritiesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    RecyclerView favoritiesRecyclerView;
    FavoritiesAdapter favoritiesAdapter;
    GridLayoutManager favoritesLayoutManager;
    List<Report> reports;
    int                 spanCount;
    static String       TAG = "Favorities";

    private OnFragmentInteractionListener mListener;

    public FavoritiesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavoritiesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoritiesFragment newInstance(String param1, String param2) {
        FavoritiesFragment fragment = new FavoritiesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_favorities, container, false);

        reports = new ArrayList<>();
        favoritiesRecyclerView = (RecyclerView) view.findViewById(R.id.favoritiesRecyclerView);
        favoritiesAdapter = new FavoritiesAdapter();

        favoritesLayoutManager = new GridLayoutManager(view.getContext(), 1);
        favoritiesAdapter.setFavorities(reports);
        favoritiesAdapter.setActivity(getActivity());
        favoritiesRecyclerView.setAdapter(favoritiesAdapter);
        favoritiesRecyclerView.setLayoutManager(favoritesLayoutManager);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateFavorities();
    }


    private void saveFavorite(int reportid){
        FavoritesSQLite favoritesSQLite=new FavoritesSQLite(this.getContext(),"DBAppSolidaridad",null,1);
        SQLiteDatabase db=favoritesSQLite.getWritableDatabase();
        db.execSQL("INSERT INTO Favorite (reportid) VALUES (" + reportid +  ")");
        db.close();
    }
    private List<String> getFavorities(){
        FavoritesSQLite favoritesSQLite=new FavoritesSQLite(this.getContext(),"DBAppSolidaridad",null,1);
        List<String> favorities = new ArrayList<>();
        String selectQuery = "SELECT * FROM Favorite" ;
        SQLiteDatabase db = favoritesSQLite.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String message="";
        if (cursor.moveToFirst()) {
            do {
                favorities.add(cursor.getString(0));
                message+=cursor.getString(0) + " ," ;

                //shopList.add(shop);
            } while (cursor.moveToNext());
        }
        return favorities;
    }

    private void updateFavorities() {
        AndroidNetworking.get(SolidaridadAppService.REPORTS_URL)
                .setTag(TAG)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        reports = Report.build(response);
                        favoritiesAdapter.setFavorities(reports);
                        favoritiesAdapter.setActivity(getActivity());
                        favoritiesAdapter.notifyDataSetChanged();


                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "Error: " + anError.getErrorBody());
                    }
                });
    }




}
