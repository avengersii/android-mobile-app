package pe.appdelasolidaridad.appdelasolidaridad.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import pe.appdelasolidaridad.appdelasolidaridad.R;
import pe.appdelasolidaridad.appdelasolidaridad.SolidaridadAppApp;
import pe.appdelasolidaridad.appdelasolidaridad.models.Session;
import pe.appdelasolidaridad.appdelasolidaridad.models.User;
import pe.appdelasolidaridad.appdelasolidaridad.services.SolidaridadAppService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyAccountFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyAccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    EditText emailEditText;
    EditText passwordEditText;
    EditText nameEditText;
    EditText lastNameEditText;
    EditText cellphoneEditText;
    Button saveButton;
    Button sendQrButton;
    Button changePassButton;
    Session session = SolidaridadAppApp.getInstance().getService().getCurrentSession();
    String TAG = "My account";

    private OnFragmentInteractionListener mListener;

    public MyAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyAccountFragment newInstance(String param1, String param2) {
        MyAccountFragment fragment = new MyAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        emailEditText = (EditText) view.findViewById(R.id.emailEditText);
        nameEditText = (EditText) view.findViewById(R.id.nameEditText);
        lastNameEditText = (EditText) view.findViewById(R.id.lastNameEditText);
        cellphoneEditText = (EditText) view.findViewById(R.id.cellphoneEditText);
        saveButton = (Button) view.findViewById(R.id.saveButton);
        sendQrButton = (Button) view.findViewById(R.id.sendQrButton);
        changePassButton = (Button) view.findViewById(R.id.changePassButton);

        final NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);

        final TextView nameSessionTextView = (TextView) hView.findViewById(R.id.nameSessionTextView);
        final TextView emailSessionTextView = (TextView) hView.findViewById(R.id.emailSessionTextView);

        emailEditText.setText(session.getUser().getEmail());
        nameEditText.setText(session.getUser().getName());
        lastNameEditText.setText(session.getUser().getLastName());
        cellphoneEditText.setText(session.getUser().getCellphone());

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObjectRequest = new JSONObject();
                try {

                    jsonObjectRequest.put("email", emailEditText.getText());
                    jsonObjectRequest.put("name", nameEditText.getText());
                    jsonObjectRequest.put("lastname", lastNameEditText.getText());
                    jsonObjectRequest.put("cellphone", cellphoneEditText.getText());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                AndroidNetworking.put(SolidaridadAppService.USERS_URL + "/{id}")
                        .setTag(TAG)
                        .addPathParameter("id",Integer.toString(session.getUser().getId()))
                        .addJSONObjectBody(jsonObjectRequest)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                User user = User.build(response);
                                session.setUser(user);

                                SolidaridadAppApp.getInstance().getService().setCurrentSession(session);
                                SharedPreferences sharedpreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                Gson gson = new Gson();
                                editor.putString("Session", gson.toJson(session));
                                editor.commit();

                                nameSessionTextView.setText(session.getUser().getName() + " " + session.getUser().getLastName());
                                emailSessionTextView.setText(session.getUser().getEmail());

                                Fragment fragment = new HomeFragment();
                                getActivity()
                                        .getSupportFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.content_main, fragment)
                                        .commit();
                            }

                            @Override
                            public void onError(ANError anError) {
                                Log.d(TAG, "Error: " + anError.getErrorBody());
                            }
                        });
            }
        });

        sendQrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new HomeFragment();
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.content_main, fragment)
                        .commit();
            }
        });

        changePassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new ChangePasswordFragment();
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.content_main, fragment)
                        .commit();
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
