package pe.appdelasolidaridad.appdelasolidaridad.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.widget.ANImageView;

import org.json.JSONObject;

import pe.appdelasolidaridad.appdelasolidaridad.R;
import pe.appdelasolidaridad.appdelasolidaridad.SolidaridadAppApp;
import pe.appdelasolidaridad.appdelasolidaridad.models.Report;
import pe.appdelasolidaridad.appdelasolidaridad.models.User;
import pe.appdelasolidaridad.appdelasolidaridad.services.SolidaridadAppService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReportFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    ANImageView imageANImageView;
    TextView dateTimeTextView;
    TextView descriptionTextView;
    TextView fullNameTextView;
    ImageButton callImageButton;
    ImageButton emailImageButton;
    Report report = SolidaridadAppApp.getInstance().getService().getCurrentReport();
    String TAG = "Report";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ReportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReportFragment newInstance(String param1, String param2) {
        ReportFragment fragment = new ReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report, container, false);

        imageANImageView = (ANImageView) view.findViewById(R.id.imageANImageView);
        dateTimeTextView = (TextView) view.findViewById(R.id.dateTimeTextView);
        descriptionTextView = (TextView) view.findViewById(R.id.descriptionTextView);
        fullNameTextView = (TextView) view.findViewById(R.id.fullNameTextView);
        callImageButton = (ImageButton) view.findViewById(R.id.callImageButton);
        emailImageButton = (ImageButton) view.findViewById(R.id.emailImageButton);

        imageANImageView.setErrorImageResId(R.mipmap.ic_launcher);
        imageANImageView.setDefaultImageResId(R.mipmap.ic_launcher);
        imageANImageView.setImageUrl(report.getImage());
        dateTimeTextView.setText(report.getDateTime());
        descriptionTextView.setText(report.getDescription());
        fullNameTextView.setText("Miguel Concepción");



        callImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "995834595"));
                startActivity(intent);
            }
        });

        emailImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","miguelsff@gmail.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Objeto perdido");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Hola Miguel por favor");
                startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUser();
    }

    private void updateUser() {
        AndroidNetworking.post(SolidaridadAppService.USERS_URL+"/{id}")
                .setTag(TAG)
                .addPathParameter("id",Integer.toString(report.getIdUser()))
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        User user = User.build(response);
                        fullNameTextView.setText("hola");
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "Error: " + anError.getErrorBody());
                    }
                });
    }
}
