package pe.appdelasolidaridad.appdelasolidaridad.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mconcepcion on 16/11/2016.
 */

public class Category {
    private int idCategory;
    private String description;
    private String image;

    public Category() {
    }

    public Category(int idCategory, String description, String image) {
        this.setIdCategory(idCategory);
        this.setDescription(description);
        this.setImage(image);
    }

    public int getIdCategory() {
        return idCategory;
    }

    public Category setIdCategory(int idCategory) {
        this.idCategory = idCategory;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Category setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getImage() {
        return image;
    }

    public Category setImage(String image) {
        this.image = image;
        return this;
    }

    public static Category build(JSONObject jsonCategory) {
        Category category = new Category();
        try {
            category.setIdCategory(jsonCategory.getInt("idcategory"))
                    .setDescription(jsonCategory.getString("description"))
                    .setImage(jsonCategory.getString("image"));
            return category;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Category> build(JSONArray jsonCategories) {
        int categoriesCount = jsonCategories.length();
        List<Category> categories = new ArrayList<>();
        for(int i = 0; i < categoriesCount; i++) {
            try {
                JSONObject jsonCategory = (JSONObject) jsonCategories.get(i);
                Category category = Category.build(jsonCategory);
                categories.add(category);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return categories;
    }

}
