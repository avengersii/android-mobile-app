package pe.appdelasolidaridad.appdelasolidaridad.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mconcepcion on 16/11/2016.
 */

public class Report {
    private Integer id;
    private String description;
    private String address;
    private String image;
    private Double latitude;
    private Double longitude;
    private Integer zoom;
    private String dateTime;
    private Integer idUser;
    private Integer idCategory;
    
    public Report() {
    }

    public Report(int idCategory, String description, String address, String image, Double latitude, Double longitude, Integer zoom, String dateTime, Integer idUser, Integer id) {
        this.setIdCategory(idCategory);
        this.setDescription(description);
        this.setAddress(address);
        this.setImage(image);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setZoom(zoom);
        this.setDateTime(dateTime);
        this.setIdUser(idUser);
        this.setId(id);
    }

    public Integer getId() {
        return id;
    }

    public Report setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Report setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Report setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getImage() {
        return image;
    }

    public Report setImage(String image) {
        this.image = image;
        return this;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Report setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Report setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public Integer getZoom() {
        return zoom;
    }

    public Report setZoom(Integer zoom) {
        this.zoom = zoom;
        return this;
    }

    public String getDateTime() {
        return dateTime;
    }

    public Report setDateTime(String dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public Report setIdUser(Integer idUser) {
        this.idUser = idUser;
        return this;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public Report setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
        return this;
    }

    public static Report build(JSONObject jsonReport) {
        Report report = new Report();
        try {
            report.setId(jsonReport.getInt("id"))
                    .setDescription(jsonReport.getString("description"))
                    .setAddress(jsonReport.getString("address"))
                    .setImage(jsonReport.getString("image"))
                    .setLatitude(Double.parseDouble(jsonReport.getString("latitude")))
                    .setLongitude(Double.parseDouble(jsonReport.getString("longitude")))
                    .setZoom(Integer.parseInt(jsonReport.getString("zoom")))
                    .setDateTime(jsonReport.getString("dateTime"))
                    .setIdUser(jsonReport.getInt("idUser"))
                    .setIdCategory(jsonReport.getInt("idCategory"));

            return report;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Report> build(JSONArray jsonReports) {
        int reportsCount = jsonReports.length();
        List<Report> reports = new ArrayList<>();
        for(int i = 0; i < reportsCount; i++) {
            try {
                JSONObject jsonReport = (JSONObject) jsonReports.get(i);
                Report report = Report.build(jsonReport);
                reports.add(report);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return reports;
    }

}
