package pe.appdelasolidaridad.appdelasolidaridad.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Miguel on 05/12/2016.
 */

public class Session {
    private boolean isLogged;
    private User user;

    public Session() {
    }

    public Session(boolean isLogged, User user) {
        this.setLogged(isLogged);
        this.setUser(user);
    }

    public boolean isLogged() {
        return isLogged;
    }

    public Session setLogged(boolean logged) {
        isLogged = logged;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Session setUser(User user) {
        this.user = user;
        return this;
    }

    public static Session build(JSONObject jsonSession, boolean isLogged) {
        Session session = new Session();
        User user = User.build(jsonSession);
        session.setLogged(isLogged)
            .setUser(user);
        return session;
    }

}
