package pe.appdelasolidaridad.appdelasolidaridad.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mconcepcion on 16/11/2016.
 */

public class User {

    private int id;
    private String email;
    private String password;
    private String name;
    private String lastName;
    private String cellphone;

    public User() {
    }

    public User(int id, String email, String password, String name, String lastName, String cellphone) {
        this.setId(id);
        this.setEmail(email);
        this.setPassword(password);
        this.setName(name);
        this.setLastName(lastName);
        this.setCellphone(cellphone);
    }

    public int getId() {
        return id;
    }

    public User setId(int id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getCellphone() {
        return cellphone;
    }

    public User setCellphone(String cellphone) {
        this.cellphone = cellphone;
        return this;
    }

    public static User build(JSONObject jsonUser){
        User user = new User();
        try {
            user.setId(jsonUser.getInt("id"))
                    .setEmail(jsonUser.getString("email"))
                    .setName(jsonUser.getString("name"))
                    .setLastName(jsonUser.getString("lastname"))
                    .setCellphone(jsonUser.getString("cellphone"));
            return user;
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("USER",e.getMessage());
        }

        return null;
    }
}
