package pe.appdelasolidaridad.appdelasolidaridad.services;

import pe.appdelasolidaridad.appdelasolidaridad.models.Category;
import pe.appdelasolidaridad.appdelasolidaridad.models.Report;
import pe.appdelasolidaridad.appdelasolidaridad.models.Session;
import pe.appdelasolidaridad.appdelasolidaridad.models.User;

/**
 * Created by mconcepcion on 01/12/2016.
 */

public class SolidaridadAppService {

    public static String CATEGORIES_URL = "http://private-7d910-appsolidaridad.apiary-mock.com/categories";
    public static String REPORTS_URL = "http://solidarityappapi.azurewebsites.net/v1//reports";
    public static String SESSION_URL = "http://solidarityappapi.azurewebsites.net/v1/users/login";
    public static String USERS_URL = "http://solidarityappapi.azurewebsites.net/v1/users";


    private Category currentCategory;
    public Category getCurrentCategory() {
        return currentCategory;
    }
    public void setCurrentCategory(Category currentCategory) {
        this.currentCategory = currentCategory;
    }

    private Session currentSession;
    public Session getCurrentSession() {
        return currentSession;
    }
    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    private Report currentReport;
    public Report getCurrentReport() {
        return currentReport;
    }
    public void setCurrentReport(Report currentReport) {
        this.currentReport = currentReport;
    }

    private User currentUser;
    public User getCurrentUser() {
        return currentUser;
    }
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }



}
