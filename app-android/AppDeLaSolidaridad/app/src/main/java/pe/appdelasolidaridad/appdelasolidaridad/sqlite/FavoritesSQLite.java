package pe.appdelasolidaridad.appdelasolidaridad.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Martel on 04/12/2016.
 */

public class FavoritesSQLite extends SQLiteOpenHelper {
    String sql="CREATE TABLE Favorite (reportid INTEGER)";
    public FavoritesSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Favorite");
        db.execSQL(sql);
    }
}
