﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Solidarityapp.API.ViewModels;
using Solidarityapp.Data.Abstract;
using Solidarityapp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.API.Controllers
{
    [Route("v1/[controller]")]
    public class ReportsController:Controller
    {
        private IReportRepository _reportRepository;
       
        int page = 1;
        int pageSize = 10;

        public ReportsController(IReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
        }


        public IActionResult Get()
        {


            IEnumerable<Report> _reports = _reportRepository
                .GetAll();

            IEnumerable<ReportViewModel> _reportVM = Mapper.Map<IEnumerable<Report>, IEnumerable<ReportViewModel>>(_reports);

            

            return new OkObjectResult(_reportVM);
        }

    }
}
