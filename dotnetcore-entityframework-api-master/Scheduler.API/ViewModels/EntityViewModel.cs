﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.API.ViewModels
{
    public class EntityViewModel : IValidatableObject
    {
        public int Id { get; set; }
        public string name { get; set; }
        public int Usercount { get; set; }
        public string Domain { get; set; }
        public string Status { get; set; }
        public ICollection<UserViewModel> Users { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new EntityViewModelValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }
    }
}
