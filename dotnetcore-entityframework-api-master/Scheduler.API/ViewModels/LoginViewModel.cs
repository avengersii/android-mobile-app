﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.API.ViewModels
{
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Oldpassword { get; set; }
        public string Newpassword { get; set; }
        public string TokenApi { get; set; }
    }
}
