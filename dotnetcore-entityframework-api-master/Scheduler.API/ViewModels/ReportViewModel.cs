﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.API.ViewModels
{
    public class ReportViewModel : IValidatableObject
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string image { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Zoom { get; set; }
        public DateTime dateTime { get; set; }
        public int? OwnerUserId { get; set; }
        public string Status { get; set; }
        public int IdUser { get; set; }
        public int IdCategory { get; set; }
        public UserViewModel OwnerUser { get; set; }
        public UserViewModel reporter { get; set; }
        public CategoryViewModel Category { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new ReportViewModelValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }
    }
}
