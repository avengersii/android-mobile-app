﻿using Solidarityapp.API.ViewModels.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Solidarityapp.API.ViewModels
{
    public class UserViewModel : IValidatableObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonProperty(Required = Required.Default)]
        public string Avatar { get; set; }
        [JsonProperty(Required = Required.Default)]
        public string Profession { get; set; }
        [JsonProperty(Required = Required.Default)]
        public int SchedulesCreated { get; set; }
        //solidarityapp changes        
        public string Email { get; set;}
        public bool ShouldSerializePassword()
        {
            return Id == 0;
        }    
        public string Password { get; set; }
        public string Lastname { get; set; }
        public string Cellphone { get; set; }
        [JsonProperty(Required =Required.Default)]
        public string Status { get; set; }
        [JsonProperty(Required = Required.Default)]
        public EntityViewModel Entity { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new UserViewModelValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }
    }
}
