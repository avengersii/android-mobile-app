﻿using Solidarityapp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.Data.Abstract
{
    public interface IScheduleRepository : IEntityBaseRepository<Schedule> { }

    public interface IUserRepository : IEntityBaseRepository<User> {   }

    public interface IAttendeeRepository : IEntityBaseRepository<Attendee> { }

    //solidarityapp changes
    public interface ICategoryRepository : IEntityBaseRepository<Category> { };

    public interface IEntityRepository : IEntityBaseRepository<Entity> { };

    public interface IReportRepository : IEntityBaseRepository<Report> { };

}
