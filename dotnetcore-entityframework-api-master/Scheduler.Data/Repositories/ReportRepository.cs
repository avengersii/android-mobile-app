﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Solidarityapp.Model;
using Solidarityapp.Data;
using Solidarityapp.Data.Repositories;
using Solidarityapp.Data.Abstract;

namespace Solidarityapp.Data.Repositories
{
    public class ReportRepository : EntityBaseRepository<Report>, IReportRepository
    {
        public ReportRepository(SolidarityappContext context)
            : base(context)
        { }
    }
}
