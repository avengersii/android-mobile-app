﻿using Solidarityapp.Data.Abstract;
using Solidarityapp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.Data.Repositories
{
    public class ScheduleRepository : EntityBaseRepository<Schedule>, IScheduleRepository
    {
        public ScheduleRepository(SolidarityappContext context)
            : base(context)
        { }
    }
}
