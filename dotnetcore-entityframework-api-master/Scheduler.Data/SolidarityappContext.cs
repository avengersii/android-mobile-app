﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Solidarityapp.Model;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Solidarityapp.Data
{
    public class SolidarityappContext : DbContext
    {
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Attendee> Attendees { get; set; }
        //SolidarityApp changes
        public DbSet<Entity> Entities { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Report> Reports { get; set; }
        

        public SolidarityappContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

/*
            modelBuilder.Entity<Schedule>()
                .ToTable("Schedule");

            modelBuilder.Entity<Schedule>()
                .Property(s => s.CreatorId)
                .IsRequired();

            modelBuilder.Entity<Schedule>()
                .Property(s => s.DateCreated)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<Schedule>()
                .Property(s => s.DateUpdated)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<Schedule>()
                .Property(s => s.Type)
                .HasDefaultValue(ScheduleType.Work);

            modelBuilder.Entity<Schedule>()
                .Property(s => s.Status)
                .HasDefaultValue(ScheduleStatus.Valid);

            modelBuilder.Entity<Schedule>()
                .HasOne(s => s.Creator)
                .WithMany(c => c.SchedulesCreated);
*/
            modelBuilder.Entity<User>()
              .ToTable("t_user");

            modelBuilder.Entity<User>()
                .Property(u => u.Email)
                .HasMaxLength(100)
                .IsRequired();
/*
            modelBuilder.Entity<Attendee>()
              .ToTable("Attendee");

            modelBuilder.Entity<Attendee>()
                .HasOne(a => a.User)
                .WithMany(u => u.SchedulesAttended)
                .HasForeignKey(a => a.UserId);

            modelBuilder.Entity<Attendee>()
                .HasOne(a => a.Schedule)
                .WithMany(s => s.Attendees)
                .HasForeignKey(a => a.ScheduleId);
*/
            //solidarityapp changes

            modelBuilder.Entity<User>()
                .HasOne(a => a.Entity)
                .WithMany(s => s.Users)
                .HasForeignKey(a => a.EntityId);



            modelBuilder.Entity<Entity>()
                .ToTable("t_entity");


            modelBuilder.Entity<Category>()
                .ToTable("t_object_category");

            modelBuilder.Entity<Report>()
                .ToTable("t_report");

            modelBuilder.Entity<Report>()
    .Property(u => u.Description)
    .HasMaxLength(300)
    .IsRequired();

/*
            modelBuilder.Entity<Report>()
                 .HasOne(a => a.OwnerUser)
                 .WithMany(s => s.Belongins)
                 .HasForeignKey(a=> a.OwnerUserId);

            modelBuilder.Entity<Report>()
                .HasOne(a => a.reporter)
                .WithMany(s => s.Reports).HasForeignKey(a => a.UserId);*/
             


        }
    }
}
