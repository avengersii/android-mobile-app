﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.Model
{
    public class Category: IEntityBase
    {
        [Column("object_category_id")]
        public int Id { get; set; }
        [Column("object_category_name")]
        public string Name { get; set;}

    }
}
