﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.Model
{
    public class Entity:IEntityBase
    {
        [Column("entity_id")]
        public int Id { get; set; }
        [Column("entity_name")]
        public string name { get; set; }
        [Column("entity_user_count")]
        public int UserCount { get; set; }
        [Column("entity_domain")]
        public string Domain { get; set; }
        [Column("entity_status")]
        public string Status { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
