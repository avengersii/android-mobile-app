﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.Model
{
    public class Report:IEntityBase
    {
        [Column("report_id")]
        public int Id { get; set; }
        [Column("report_description")]
        public string Description { get; set; }
        [Column("report_address")]
        public string Address { get; set; }
        [Column("report_photo_uri")]
        public string PhotoUri { get; set; }
        [Column("report_latitude")]
        public double Latitude { get; set; }
        [Column("report_longitude")]
        public double Longitude { get; set; }
        [Column("report_zoom")]
        public int Zoom { get; set; }
        [Column("report_date")]
        public DateTime date { get; set; }
        [Column("report_owner_user_id")]
        public int? OwnerUserId { get; set; }
        [Column("report_status",TypeName ="char(3)")]
        public string Status { get; set; }
        [Column("user_id")]
        public int UserId { get; set; }
        [Column("object_category_id")]
        public int CategoryId { get; set; }
        [NotMapped]
        public User OwnerUser { get; set; }
        [NotMapped]
        public User reporter { get; set; }
        [NotMapped]
        public Category Category { get; set; }
    }
}
