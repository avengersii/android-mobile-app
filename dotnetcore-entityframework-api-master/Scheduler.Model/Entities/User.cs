﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Solidarityapp.Model
{
    public class User : IEntityBase
    {
        public User()
        {
            //SchedulesCreated = new List<Schedule>();
            //SchedulesAttended = new List<Attendee>();
            Belongins = new List<Report>();
            Reports = new List<Report>();
        }
        [Column("user_id")]
        public int Id { get; set; }
        [Column("user_name")]
        public string Name { get; set; }
        /*public string Avatar { get; set; }
        public string Profession { get; set; }
        public ICollection<Schedule> SchedulesCreated { get; set; }
        public ICollection<Attendee> SchedulesAttended { get; set; }*/
        //solidarityapp changes
        [Column("user_email")]
        public string Email { get; set; }
        [Column("user_password")]
        public string Password { get; set; }
        [Column("user_last_name")]
        public string LastName { get; set; }
        [Column("user_cellphone")]
        public string Cellphone { get; set; }
        [Column("user_status")]
        public string Status { get; set; }
        [Column("entity_id")]
        public int EntityId { get; set; }
        [NotMapped]
        public ICollection<Report> Belongins { get; set; }
        public Entity Entity { get; set; }
        [NotMapped]
        public ICollection<Report> Reports { get; set; }
    }
}
